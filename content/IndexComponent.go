package content

import "time"

//go:generate gwf gen component IndexComponent

type IndexComponent struct {
	Title   string
	Name    string
	Created time.Time
}

// New is used to create a new IndexComponent with default attributes
func New() *IndexComponent {
	return &IndexComponent{Title: "GWF Application", Name: "world", Created: time.Now()}
}
