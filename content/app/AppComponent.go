package app

import "time"

//go:generate gwf gen component AppComponent
type AppComponent struct {
	Time time.Time
}

func New() *AppComponent {
	return &AppComponent{
		Time: time.Now(),
	}
}
