package sysinfo

import "time"

//go:generate gwf gen component SysinfoComponent
type SysinfoComponent struct {
	Service *SysinfoService
}

func New(service *SysinfoService) *SysinfoComponent {
	return &SysinfoComponent{Service: service}
}

func (component *SysinfoComponent) GetTime() time.Time {
	return component.Service.GetTime()
}
