package sysinfo

import "time"

// go:generate gwf generate service -name="SysinfoService"
type SysinfoService struct{}

func NewSysinfoService() *SysinfoService {
	return &SysinfoService{}
}

func (service *SysinfoService) GetTime() time.Time {
	return time.Now()
}
