package main

import (
	"gitlab.com/gwf/lib"
	"log"
	"net/http"
	"time"
)

type handler struct{}

func (h *handler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	start := time.Now()
	defer func() {
		took := time.Since(start)
		log.Printf("Took %v for request to path: %s\n", took, r.RequestURI)
	}()
	if r.RequestURI == "/favicon.ico" {
		http.ServeFile(w, r, "static/favicon.ico")
		return
	}

	// Add all available template of Components
	ctx := &lib.RenderContext{
		Path:       r.RequestURI,
		Components: Application.Components,
		Routes:     Application.Routes,
		Wirer:      Wirer,
	}

	rendered := Application.Root.Instance.Render(ctx)
	_, err := w.Write([]byte(rendered))
	if err != nil {
		log.Fatalf("failed to write response: %v", err)
	}
}

func main() {
	log.Printf("Serving @ http://localhost:8080")

	h := &handler{}
	fs := http.FileServer(http.Dir("static/"))

	mux := http.NewServeMux()
	mux.Handle("/", h)
	mux.Handle("/static/", http.StripPrefix("/static/", fs))

	err := http.ListenAndServe(":8080", mux)
	if err != nil {
		log.Fatalf("finished serving with error: %v", err)
	}
}
