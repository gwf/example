package main

import (
	cont "gitlab.com/gwf/example/content"
	"gitlab.com/gwf/example/content/app"
	"gitlab.com/gwf/example/content/login"
	"gitlab.com/gwf/example/content/sysinfo"
	"gitlab.com/gwf/lib"
	"gitlab.com/gwf/lib/components/pagenotfound"
	"gitlab.com/gwf/lib/components/router"
)

var Application = lib.Application{
	Root: &lib.EntryPoint{
		Spec:     cont.Spec,
		Instance: cont.New(),
	},
	Components: lib.ComponentList{
		// Add every component here
		cont.Spec,
		app.Spec,
		login.Spec,
		sysinfo.Spec,
		router.Spec,
		pagenotfound.Spec,
	},
	Routes: lib.NewRoutes([]*lib.RawRouteEntry{
		{Regex: "/login", Spec: login.Spec},
		{Regex: "/sysinfo", Spec: sysinfo.Spec},
		{Regex: "/.*", Spec: app.Spec},
		// Only required if no "pagenotfound" component is registered
		{Regex: "/.*", Spec: pagenotfound.Spec},
	}),
}
