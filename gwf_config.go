package main

import (
	"github.com/google/wire"
	"gitlab.com/gwf/example/content/sysinfo"
)

//go:generate gwf create wirer

// Wire is used to build initializers for components.
// Disadvantage: You have to build the dependency tree of the components yourself (build through `gwf build`)

func InitializeSysinfoService() (*sysinfo.SysinfoService, error) {
	panic(wire.Build(sysinfo.NewSysinfoService))
}
