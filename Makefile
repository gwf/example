PKG_LIST := $(shell go list ./... | grep -v /vendor/)
GO_FILES := $(shell find . -name '*Component.go' | grep -v /vendor/ | grep -v _test.go | grep -v .gen.go)

generate:
	for file in $(GO_FILES); do \
	go generate $$file; \
	done
	go generate

serve: generate
	@go run main.go main_gen.go wire_gen.go

build: generate
	@go build -o build/example