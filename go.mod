module gitlab.com/gwf/example

go 1.13

require (
	github.com/google/wire v0.4.0
	gitlab.com/gwf/lib v0.0.0-20200103173108-54bbaa3e312e
)
